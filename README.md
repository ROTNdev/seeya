Seeya
=====

A punishment/banning plugin supporting SQL and UUIDs.

### What comes in the box?

- The Seeya Plugin
- Fancy web interface to manage punishments [**Demo**](http://bans.linkr.pw)

### Prerequisites

- [**Lombok**](http://projectlombok.org/)
- [**Bukkit API**](http://dl.bukkit.org/)