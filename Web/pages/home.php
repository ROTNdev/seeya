<?php 
require_once("../assets/config.php"); 

buildHeader();?>
<!DOCTYPE html>
	<body>
		<?php sendNav();?>
		<div class="container">
			<h1>Seeya <small>SQL Punishment System</small></h1>
			<hr>
			<p>To browse punishments click the links above</p>
		</div>
		<?php sendFooter(); ?>
	</body>
</html>