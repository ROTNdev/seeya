<?php 
require_once("../assets/config.php"); 

buildHeader("Not Found");?>
<!DOCTYPE html>
	<body>
		<?php sendNav();?>
		<div class="container">
			<h1>404 not found</h1>
			<hr>
			<p>The page you requested could either not be found or you can't access it.</p>
		</div>
	</body>
</html>