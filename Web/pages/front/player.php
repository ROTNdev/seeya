<?php 
require_once("../../assets/config.php");

if(empty($_GET)){
	header("Location: ".PANEL_DIR);
	die();
}

$name = $_GET["name"];
$name = fixName($name);
$nonExistant = false;

if($name == "." || empty($name)){
	$nonExistant = true;
}

if($_GET["name"] != $name && !$nonExistant){
	redirect(PANEL_DIR."/player/".$name);
}

buildHeader($name == "." ? "Not Found" : $name);?>
<!DOCTYPE html>
	<body>
		<?php sendNav($name);?>
		<div class="container">
			<?php if($nonExistant){
				echo '<h3>Not found</h3>
				<hr>
				'.$_GET["name"].' has never played minecraft (according to mojang.com)';
				die();
			} ?>
			<h1><img src="https://mcavatar.pw/a/<?php echo $name; ?>/40"> <?php echo $name; ?></h1>
			<hr>
			<div class="row text-center">
				<div class="col-md-6">
					<div class="panel panel-warning">
						<div class="panel-heading">Kicks that pertain <?php echo $name; ?></div>
						<div class="panel-body text-center">
							<span id="kicks">
								<img src="../../assets/ajax-loader.gif">
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-info">
						<div class="panel-heading">Mutes that pertain <?php echo $name; ?></div>
						<div class="panel-body text-center">
							<span id="mutes">
								<img src="../../assets/ajax-loader.gif">
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel panel-danger">
						<div class="panel-heading">Bans that pertain <?php echo $name; ?></div>
						<div class="panel-body text-center">
							<span id="bans">
								<img src="../../assets/ajax-loader.gif">
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php sendFooter(); ?>
		<script type="text/javascript">
			$(document).ready(function(){
				getAll();
				setInterval(getAll, 1000 * 60 * 5);
			});
			function getAll(){
				get('bans');
				get('mutes');
				get('kicks');
			}
			function get(type){
				$('#'+type).html('<img src="../../assets/ajax-loader.gif">');
				$('#'+type).load('<?php echo PANEL_DIR ?>/ajax/player.php?type='+type+'&name=<?php echo $name; ?>');
			}
		</script>
	</body>
</html>