<?php 
require_once("../../assets/config.php");

buildHeader("Mutes");?>
<!DOCTYPE html>
	<body>
		<?php sendNav("Mutes");?>
		<div class="container">
			<h2>Mutes <small>Feature not implemented</h2>
			<hr>
			<h5>Because this feature has not been implemented into the bukkit plugin obviously it can't be displayed here.<br>
				Expect an update including /mute, /unmute and other bug fixes+additions soon. Check the bukkitdev page <a href="https://linkr.pw/seeya">here</a> frequently.</h5>
		</div>
		<?php sendFooter(); ?>
	</body>
</html>