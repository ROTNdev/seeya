<?php 
require_once("../../assets/config.php"); 

if(!empty($_POST["name"]) && !empty($_POST["ptype"])){
	$name = $mysqli->real_escape_string($_POST["name"]);
	$type = $mysqli->real_escape_string($_POST["ptype"]);// Escape just incase
	$activeOnly = $_POST["active"] == "active";
	if($activeOnly && $type == "kicks"){
		$activeOnly = false;
	}
	$uuid = resolveUUID($name);

	$activeField = $type == "bans" ? "unbanned" : "active";

	$query = $mysqli->query("SELECT * FROM ".DATABASE_PREFIX.$type." WHERE uuiduser = '".$uuid."'".($activeOnly ? " AND ".$activeField." = ".($activeField == "unbanned" ? 0 : 1)."" : "").";");

	if($query->num_rows <= 0){
		$empty = true;
	} else {
		$process = true;
	}
}

buildHeader("Search");?>
<!DOCTYPE html>
	<body>
		<?php sendNav("Search");?>
		<div class="container">
			<h2>Find a player <?php if($process) { echo '<small>Search Results</small>'; } ?></h2>
			<hr>
			<?php
				if($empty){
					echo '<div class="span5 alert alert-danger text-center">Nothing found with the given criteria</div>';
				}
				if($process){
					?>
					<table class="table table-striped table-bordered">
						<thead>
							<td>Username</td>
							<td>When</td>
							<td>Punisher</td>
							<?php
								if(!$activeOnly){ echo '<td>Active?</td>';}
							?>
						</thead>
						<tbody>
							<?php
								while($row = mysqli_fetch_array($query)){
									echo '<tr>';
									echo '<td><img src="https://mcavatar.pw/a/'.$row["username"].'/26.png" class="avatar"> <a id="tablelink" href="'.PANEL_DIR.'/player/'.$row["username"].'">'.$row["username"].'</a></td>';
									echo '<td>'.timeAgo($row["time"]).'</td>';
									echo '<td><img src="https://mcavatar.pw/a/'.$row["punisher"].'/26.png" class="avatar"> <a id="tablelink" href="'.PANEL_DIR.'/player/'.$row["punisher"].'">'.$row["punisher"].'</a></td>';
									if(!$activeOnly){
										echo '<td>'.($row[$activeField] == 1 ? "No" : "Yes").'</td>';
									}
									echo '</tr>';
								}
							?>
						</tbody>
					</table>

					<?php
				} else {

			?>
			<form method="post">
				<div>
					<label for="name">
						Player name to search
					</label>
					<input class="form-control span4" type="text" name="name" max-length="16" placeholder="Username" <?php if(isset($name)){ echo 'value="'.$name.'"'; } ?> autofocus required>
				</div>
				<br>
				<div>
					<label for="ptype">
						Punishment type you're looking for:
					</label>
					<select class="form-control span3" name="ptype" required>
						<option value="bans">Ban or Tempban</option>
						<option value="kicks">Kick</option>
						<option value="mutes">Mute</option>
					</select>
				</div>
				<br>
				<div class="checkbox">
                    <label>
                        <input type="checkbox" name="active" value="active"> Do you want active punishments only?
                    </label>
            	</div>
				<br>
				<button class="btn btn-block span3 btn-success" type="submit">Search &raquo;</button>
			</form>
			<?php } ?>
		</div>
		<?php sendFooter(); ?>
	</body>
</html>