<?php 
require_once("../../assets/config.php");

// GET prcessing:
$server = "All";
if(isset($_GET["server"])){
	$server = $_GET["server"];
}

$limit = 50;
if(isset($_GET["limit"])){
	$limit = $_GET["limit"];
}

$sort = "id";
if(isset($_GET["sort"])){
	$sort = $_GET["sort"];
}

buildHeader("Bans");?>
<!DOCTYPE html>
	<body>
		<?php sendNav("Bans");?>
		<div class="container">
			<h2>Bans <small>Server- <?php echo $server; ?></small></h2>
			<hr>
			<div id="data">
				<img style="display: block; margin-left: auto; margin-right: auto;" src="../../assets/ajax-loader.gif">
			</div>
		</div>
		<?php sendFooter(); ?>
		<script type="text/javascript">
			$(document).ready(function(){
				getData('<?php echo $server."','".$limit."','".$sort; ?>')
			});
		function getData(server, limit, sort){
			$('#data').html('<img style="display: block; margin-left: auto; margin-right: auto;" src="../../assets/ajax-loader.gif">');
			$('#data').load('<?php echo PANEL_DIR ?>/ajax/bans.php?server='+server+'&limit='+limit+'&sort='+sort);
		}
	</script>
	</body>
</html>