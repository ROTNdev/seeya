<?php

// Set your database connection credentials - make sure it is the same database as the bukkit plugin
define('DATABASE_HOST', "localhost"); // Database host/url/ip
define('DATABASE_USER', "root"); // Database username
define('DATABASE_PASS', "password"); // Database password
define('DATABASE_NAME', "minecraft"); // Database name
define('DATABASE_PREFIX', 'seeya_'); // The prefix of the tables

// DO NOT EDIT THIS:
$mysqli = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);
global $mysqli;

$connectionError = "";
if($mysqli->connect_errno){
	$connectionError = $mysqli->connect_error;
}
global $connectionError;

?>