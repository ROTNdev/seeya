<?php 

// Initialize the database connection.
require_once("database.php");

// Define variables
define("PANEL_TITLE", "Seeya"); // The title of this UI/Panel
define("PANEL_DIR", "http://bans.linkr.pw");// The location of this UI - do NOT include the trailing slash
define("PANEL_STYLE", "bootstrap.css");// Main CSS file this panel will use you can edit the header below to add multiple files.

//
// *+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+* //
//

require_once("funcs/funcs-gen.php");
require_once("funcs/funcs-minecraft.php");

session_start();

/**
 * Send title, css and other resources
 * FontAwesome is added for icons
 */
function buildHeader($currentPage = "Home", $redirect = "/"){
	// Check if user can access page (disabled currently)
	//checkPermissions($currentPage, $redirect);
	?>
	<head>
		<title><?php echo str_replace("_", " ", $currentPage)." &middot; ".PANEL_TITLE; ?></title>
	    <link rel="stylesheet" type="text/css" href="<?php echo PANEL_DIR."/assets/css/".PANEL_STYLE; ?>">
	    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<style>
			p,h1,h2,h3,h4,h5,body{
				font-family: 'Open Sans', sans-serif;
			}
			.avatar{
		        border-radius:3px;
		        -webkit-border-radius:3px;
		        -moz-border-radius:3px;
        		display:inline-block
      		}
		</style>
	</head><?php
}

$navPages = array('Search' => '<i class="fa fa-search"></i>',
 'Bans' => '<i class="fa fa-thumbs-o-down"></i>',
  'Kicks' => '<i class="fa fa-exclamation"></i>',
  'Mutes' => '<i class="fa fa-volume-off"></i>');

function sendNav($currentPage = "Home"){
	?>
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo PANEL_DIR; ?>"><?php echo PANEL_TITLE; ?></a>
	    </div>
	    <div class="collapse navbar-collapse navbar-ex1-collapse">
	      <ul class="nav navbar-nav pull-right">
	      	<?php
	      	global $navPages;
	      	foreach ($navPages as $page => $icon) {
	      		if($currentPage == $page){
	      			echo '<li class="active"><a href="'.PANEL_DIR.'/'.strtolower($page).'">'.$icon.' '.$page.'</a></li>';
	      		} else {
	      			echo '<li><a href="'.PANEL_DIR.'/'.strtolower($page).'">'.$icon.' '.$page.'</a></li>';
	      		}
	      	}

	      	?>
	    </div>
	  </div>
	</nav>
<?php
global $connectionError;
if(!empty($connectionError)){
	echo '<div class="container"><h2>Connection to database failed. '.$connectionError.'</h2></div>';
	die();
}
}

function sendFooter(){?>
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<?php
}

?>