<?php

function resolveUUID($name){

    $arr = array("name"=>$name, "agent"=>"minecraft");
    $data = json_encode($arr);

    $ch = curl_init('https://api.mojang.com/profiles/page/1');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data))
    );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    if(!($res = curl_exec($ch))) {
    //echo('[cURL Failure] ' . curl_error($ch));
    }

    curl_close($ch);
    $username = json_decode($res, true);

    return $username['profiles']['0']['id'];
}

function fixName($name){

    $arr = array("name"=>$name, "agent"=>"minecraft");
    $data = json_encode($arr);

    $ch = curl_init('https://api.mojang.com/profiles/page/1');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data))
    );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    if(!($res = curl_exec($ch))) {
    //echo('[cURL Failure] ' . curl_error($ch));
        $fail = true;
    }

    curl_close($ch);
    $username = json_decode($res, true);

    if($fail){
        return ".";
    } else {
        return $username['profiles']['0']['name'];
    }
}

?>