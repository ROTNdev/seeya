<?php

/*
 * Redirect to a specified page, then die.
 */
function redirect($toPage){
	header('Location: '.$toPage);
	die();
}

function timeAgo($inputTime){
	$time = time() - $inputTime;

	if($time < 1){
		return "now";
	}

	$a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
                );

	foreach ($a as $secs => $str) {
		$d = $time / $secs;
		if($d >= 1){
			$rounded = round($d);
			return $rounded.' '.$str.($rounded > 1 ? 's' : '') . ' ago';
		}
	}
}

/*
 * Convert seconds to human readable text.
 * http://csl.sublevel3.org/php-secs-to-human-text/
 */
function secs_to_h($secs) {
	$units = array(
		"week"   => 7*24*3600,
		"day"    =>   24*3600,
		"hour"   =>      3600,
		"minute" =>        60,
		"second" =>         1,
	);

	// specifically handle zero
	if ( $secs == 0 )
		return "0 seconds";
	$s = '';
	foreach ( $units as $name => $divisor ) {
		if ( $quot = intval($secs / $divisor) ) {
			$s .= "$quot $name";
			$s .= (abs($quot) > 1 ? "s" : "") . ", ";
			$secs -= $quot * $divisor;
		}
	}
	return substr($s, 0, -2);
}

?>