<?php
require_once("../assets/config.php");

global $mysqli;
$type = $_GET["type"];
$name = $mysqli->real_escape_string($_GET["name"]);

if(empty($type) || ($type != "bans" && $type != "kicks" && $type != "mutes")){
	echo '<b>Nothing found</b>';
	die();
}

$query = $mysqli->query("SELECT * FROM ".DATABASE_PREFIX.$type." WHERE username = '".$name."';");

if($query->num_rows <= 0){
	echo '<b>Nothing found</b>';
	die();
}

echo '<table class="table table-striped table-hover">';
if($type == "bans"){
	echo '<th>ID</th>';
	echo '<th>Punisher</th>';
	echo '<th>When</th>';
	echo '<th>Reason</th>';
	echo '<th>Permanent?</th>';
	echo '<th>Active?</th>';

	while($row = mysqli_fetch_array($query)){
		echo '<tr>';
		echo '<td>'.$row["id"].'</td>';
		echo '<td><img src="https://mcavatar.pw/a/'.$row["punisher"].'/15.png"> <a href="'.PANEL_DIR.'/player/'.$row["punisher"].'">'.$row["punisher"].'</a></td>';
		echo '<td>'.timeAgo($row["time"]).'</td>';
		echo '<td>'.$row["reason"].'</td>';
		echo '<td>'.($row["duration"] == 0 ? "Yes":"No").'</td>';
		echo '<td>'.($row["unbanned"] == 1 ? "No":"Yes").'</td>';
		echo '</tr>';
	}
} else if($type == "kicks"){
	echo '<th>ID</th>';
	echo '<th>Punisher</th>';
	echo '<th>When</th>';
	echo '<th>Reason</th>';

	while($row = mysqli_fetch_array($query)){
		echo '<tr>';
		echo '<td>'.$row["id"].'</td>';
		echo '<td><img src="https://mcavatar.pw/a/'.$row["punisher"].'/15.png"> <a href="'.PANEL_DIR.'/player/'.$row["punisher"].'">'.$row["punisher"].'</a></td>';
		echo '<td>'.timeAgo($row["time"]).'</td>';
		echo '<td>'.$row["reason"].'</td>';
		echo '</tr>';
	}
} else if($type == "mutes"){
	echo '<th>ID</th>';
	echo '<th>Punisher</th>';
	echo '<th>When</th>';
	echo '<th>Reason</th>';
	echo '<th>Active?</th>';

	while($row = mysqli_fetch_array($query)){
		echo '<tr>';
		echo '<td>'.$row["id"].'</td>';
		echo '<td><img src="https://mcavatar.pw/a/'.$row["punisher"].'/15.png"> <a href="'.PANEL_DIR.'/player/'.$row["punisher"].'">'.$row["punisher"].'</a></td>';
		echo '<td>'.timeAgo($row["time"]).'</td>';
		echo '<td>'.$row["reason"].'</td>';
		echo '<td>'.($row["active"] == 1 ? "Yes":"No").'</td>';
		echo '</tr>';
	}
}

echo'</table>';
?>