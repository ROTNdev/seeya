<?php
require_once("../assets/config.php");
?>

<table class="table table-striped table-bordered">
	<?php
		$server = "all";
		if(isset($_GET["server"])){
			$server = strtolower($mysqli->real_escape_string($_GET["server"]));
		}

		$limit = 50;
		if(isset($_GET["limit"])){
			$limit = $mysqli->real_escape_string($_GET["limit"]);
		}

		$sort = "id";
		if(isset($_GET["sort"])){
			$sort = $mysqli->real_escape_string($_GET["sort"]);
		}?>
	<thead>
		<td><a href="<?php echo PANEL_DIR."/kicks?sort=id"; ?>">ID</a></td>
		<td><a href="<?php echo PANEL_DIR."/kicks?sort=username"; ?>">Username</a></td>
		<td><a href="<?php echo PANEL_DIR."/kicks?sort=punisher"; ?>">Punisher</a></td>
		<td>Reason</td>
		<td>When</td>
		<?php
		if($server == "all"){
			echo '<td><a href="'.PANEL_DIR.'/kicks?sort=server">Server</a></td>';
		}
		?>
	</thead><?php
		$query = $mysqli->query("SELECT * FROM ".DATABASE_PREFIX."kicks ".($server == "all" ? "" : "WHERE server = '".$server."' ")."ORDER BY ".$sort." ASC LIMIT ".$limit);
		if($query->num_rows > 0){
			while ($row = mysqli_fetch_array($query)) {
				echo '<tr>';
				echo '<td>'.$row["id"].'</td>';
				echo '<td><img class="avatar" src="https://mcavatar.pw/a/'.$row["username"].'/15.png"> <a href="'.PANEL_DIR.'/player/'.$row["username"].'">'.$row["username"].'</a></td>';
				echo '<td><img class="avatar" src="https://mcavatar.pw/a/'.$row["punisher"].'/15.png"> <a href="'.PANEL_DIR.'/player/'.$row["punisher"].'">'.$row["punisher"].'</a></td>';
				echo '<td>'.$row["reason"].'</td>';
				echo '<td>'.timeAgo($row["time"]).'</td>';
				if($server == "all"){
					echo '<td>'.$row["server"].'</td>';
				}
				echo '</tr>';
			}
		} else { ?>
	<tbody>
	</tbody>
</table>
<center><h3>No results found</h3></center>
<?php } ?>