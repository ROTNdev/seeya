package pw.linkr.bukkit.seeya.config;

import lombok.Getter;

public class Lang extends Config {

	public Lang() {
		super("lang");

		getConfig().addDefault("kicks.kick-msg", "&bYou've been kicked for %reason");
		getConfig().addDefault("bans.ban-msg", "&bYou've been banned for %reason");
		getConfig().addDefault("bans.tempban-msg", "&bYou've been temp-banned for %reason \n (%time)");
		getConfig().addDefault("bans.join-while-banned", "&bYou're currently banned for %reason");
		getConfig().addDefault("bans.join-while-temp", "&bYou're currently banned for %reason \n &bTime remaining: &e%time");
		getConfig().options().copyDefaults(true);
		save();
		
		banJoin = getConfig().getString("bans.join-while-banned");
		tempBanJoin = getConfig().getString("bans.join-while-temp");
		ban = getConfig().getString("bans.ban-msg");
		tempBan = getConfig().getString("bans.tempban-msg");
		kick = getConfig().getString("kicks.kick-msg");
	}

	@Getter private String banJoin, tempBanJoin, kick, ban, tempBan;

}
