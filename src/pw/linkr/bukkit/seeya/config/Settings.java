package pw.linkr.bukkit.seeya.config;

import lombok.Getter;
import pw.linkr.bukkit.seeya.Punishment.PunishmentType;

public class Settings extends Config {

	public Settings() {
		super("settings");

		getConfig().addDefault("info.server-name", "your-server");

		getConfig().addDefault("sql.host", "localhost");
		getConfig().addDefault("sql.prefix", "seeya_");
		getConfig().addDefault("sql.pass", "password");
		getConfig().addDefault("sql.user", "root");
		getConfig().addDefault("sql.database", "minecraft");
		getConfig().addDefault("sql.port", 3306);
		getConfig().options().copyDefaults(true);
		save();
		
		server = getConfig().getString("info.server-name");
		
		sql_database_name = getConfig().getString("sql.database");
		sql_host = getConfig().getString("sql.host");
		sql_port = getConfig().getInt("sql.port");
		sql_prefix = getConfig().getString("sql.prefix");
		sql_user = getConfig().getString("sql.user");
		sql_pass = getConfig().getString("sql.pass");
		
		PunishmentType.init(getConfig());
	}

	@Getter private String server, sql_database_name, sql_host, sql_pass, sql_user, sql_prefix;
	@Getter private int sql_port;

}
