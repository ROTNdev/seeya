package pw.linkr.bukkit.seeya.config;

import java.io.File;
import java.io.IOException;

import lombok.Getter;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pw.linkr.bukkit.seeya.Seeya;

public abstract class Config {

	@Getter private File file;
	@Getter private FileConfiguration config;
	@Getter private String fileName;

	/**
	 * Create a config file
	 * @param name - File name
	 */
	public Config(String name) {
		this.fileName = name;

		try {
			
			File dir = new File(Seeya.getInstance().getDataFolder().getAbsolutePath());
			if(!dir.exists()){
				dir.mkdir();
			}
			
			this.file = new File(Seeya.getInstance().getDataFolder(), name+".yml");

			if(!file.exists()){
				file.createNewFile();
			}

			save();
		} catch(IOException ex) { ex.printStackTrace(); }
	}

	public void save() {
		try {
			if(config == null){
				config = YamlConfiguration.loadConfiguration(file);
			}

			config.save(file);

			config = YamlConfiguration.loadConfiguration(file);
		} catch(IOException ex) { ex.printStackTrace(); }
	}

}