package pw.linkr.bukkit.seeya;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lombok.Getter;

public class SQL {

	@Getter private Connection connection = null;

	public SQL(String host, String pass, String user, String database, int port) {
		long start = 0, end = 0;

		try {
			start = System.currentTimeMillis();
			Seeya.getInstance().logMessage(true, false, "Connecting to SQL..");
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true", user, pass);
			end = System.currentTimeMillis();
			System.out.println("Connected, (took " + ((end - start) / 1000) + ")");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection failed, reason: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver not found!");
		}
	}

	public boolean isConnected() {
		if(connection != null) {
			return true;
		}

		return false;
	}

	public void closeConnection() {
		try {
			if(isConnected()) {
				connection.close();
				connection = null;
			}
		} catch(Exception e) { }
	}

	/**
	 * Send a query to database
	 * @param sql - query to send
	 * @param close - should the query be closed after the resultset is returned
	 * @return -  result set from query or NULL
	 */
	public ResultSet query(String sql, boolean close) {
		ResultSet rs = null;

		try {
			Statement statement = connection.createStatement();
			rs = statement.executeQuery(sql);

			if(close) {
				statement.close();
			}
		} catch (SQLException e) { }

		return rs;
	}

	/**
	 * Send a query to database<br>
	 * <b>Set is not closed</b>
	 * @param sql - query to send
	 * @return a result set from query or NULL
	 */
	public ResultSet query(String sql){
		return query(sql, false);
	}

	/**
	 * Send an update to database
	 * @param sql - update to send
	 */
	public int update(String sql) {
		int st = 0;

		try {
			Statement statement = connection.createStatement();
			st = statement.executeUpdate(sql);
			statement.close();
		} catch (SQLException e) { e.printStackTrace(); Seeya.getInstance().logMessage(true, true, "An error occured (sql->update)", "Please read the above stacktrace for more information", "Do not report this to the plugin developers unless you are certain it is not a problem on your end."); }

		return st;
	}

	/**
	 * Get the amount of rows in a table
	 * @param table to get count from
	 * @return the row count
	 */
	public int getRowCount(String table) {
		int count = 0;

		try {
			ResultSet rs = query("SELECT COUNT(*) FROM " + table);

			if(rs.next()) {
				count = rs.getInt("COUNT(*)");
			}
		} catch(Exception e) { }

		return count;
	}

	public void buildTables() {
		String prefix = Seeya.getSettings().getSql_prefix();
		update("CREATE TABLE IF NOT EXISTS `" + prefix + "bans` (`id` int(255) NOT NULL AUTO_INCREMENT, `username` varchar(16) NOT NULL, `punisher` varchar(16) NOT NULL, `reason` text NOT NULL, `time` bigint(255) NOT NULL, `duration` int(255) NOT NULL, `unbanned` int(255) NOT NULL, `unbantime` int(255) NOT NULL, `unbanner` varchar(32) NOT NULL, `server` varchar(255) NOT NULL, `uuiduser` text NOT NULL, `uuidpunisher` text NOT NULL, PRIMARY KEY (`id`))");
		update("CREATE TABLE IF NOT EXISTS `" + prefix + "kicks` (`id` int(255) NOT NULL AUTO_INCREMENT, `username` varchar(16) NOT NULL, `punisher` varchar(16) NOT NULL, `reason` text NOT NULL, `time` bigint(255) NOT NULL, `server` varchar(255) NOT NULL, `uuiduser` text NOT NULL, `uuidpunisher` text NOT NULL, PRIMARY KEY (`id`))");
		update("CREATE TABLE IF NOT EXISTS `" + prefix + "mutes` (`id` int(255) NOT NULL AUTO_INCREMENT, `username` varchar(16) NOT NULL, `punisher` varchar(16) NOT NULL, `reason` text NOT NULL, `time` bigint(255) NOT NULL, `duration` int(255) NOT NULL, `active` int(255) NOT NULL, `server` varchar(255) NOT NULL, `uuiduser` text NOT NULL, `uuidpunisher` text NOT NULL, PRIMARY KEY (`id`))");
	}

}
