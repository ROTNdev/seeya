package pw.linkr.bukkit.seeya;

import java.util.Collection;

import lombok.Getter;

import org.bukkit.plugin.java.JavaPlugin;

import pw.linkr.bukkit.seeya.commands.punish.BanCMD;
import pw.linkr.bukkit.seeya.commands.punish.KickCMD;
import pw.linkr.bukkit.seeya.commands.punish.SeeyaCMD;
import pw.linkr.bukkit.seeya.commands.punish.SeeyaInfoCMD;
import pw.linkr.bukkit.seeya.commands.punish.TempBanCMD;
import pw.linkr.bukkit.seeya.commands.punish.UnbanCMD;
import pw.linkr.bukkit.seeya.config.Lang;
import pw.linkr.bukkit.seeya.config.Settings;
import pw.linkr.bukkit.seeya.listener.CallListener;
import pw.linkr.bukkit.seeya.util.PunishmentCache;

public class Seeya extends JavaPlugin{

	@Getter private static Seeya instance;
	@Getter private static SQL SQL;
	/**
	 * Language file (contains strings)
	 */
	@Getter private static Lang lang;
	/**
	 * Plugin settings
	 */
	@Getter private static Settings settings;

	public void reloadConfigs(){
		lang = new Lang();
		settings = new Settings();
	}

	@Override
	public void onEnable() {
		instance = this;
		lang = new Lang();
		settings = new Settings();

		SQL = new SQL(settings.getSql_host(), settings.getSql_pass(), settings.getSql_user(), settings.getSql_database_name(), settings.getSql_port());
		if(SQL.isConnected()){
			SQL.buildTables();

			boolean error = false;

			try {
				PunishmentCache.getInstance().fetchAll(SQL);
			} catch (Exception ex) {
				error = true;
				ex.printStackTrace();
				logMessage(false, true, "An error occured during startup (cache->fetchAll)", "Please read the above stacktrace for more information", "Do not report this to the plugin developers unless you are certain it is not a problem on your end.");
				return;
			}

			if(!error){
				getServer().getPluginManager().registerEvents(new CallListener(), this);
				getCommand("kick").setExecutor(new KickCMD());
				getCommand("ban").setExecutor(new BanCMD());
				getCommand("unban").setExecutor(new UnbanCMD());
				getCommand("tempban").setExecutor(new TempBanCMD());
				getCommand("seeya").setExecutor(new SeeyaCMD());
				getCommand("seeyainfo").setExecutor(new SeeyaInfoCMD());
				logMessage(false, true, "Seeya has enabled");
			}
		} else {
			logMessage(false, true, "An error occured during startup (sql->connect)", "The connection to SQL failed.", "Check the credentials in your settings.yml and try again.");
		}
		super.onEnable();
	}

	/**
	 * Send a bordered message to console - mostly for debug or information
	 * @param top - Should the top ----[ Seeya ]--- line be shown?
	 * @param bottom Should the bottom ----[ Seeya ]--- line be shown?
	 * @param msg The message to send.
	 */
	public void logMessage(boolean top, boolean bottom, String... msg){
		if(top){
			System.out.println("----[ Seeya ]----");
		}
		for(String str : msg){
			System.out.println(str);
		}
		if(bottom){
			System.out.println("----[ Seeya ]----");
		}
	}

	@Override
	public void onDisable() {
		SQL.closeConnection();
		super.onDisable();
	}

//	public static Seeya getInstance() {
//		return (Seeya)Bukkit.getPluginManager().getPlugin("Seeya");
//	}

	/**
	 * Join an array into a string (separated by a beautiful space)
	 */
	public String buildArgs(String[] args, int start){
		StringBuilder builder = new StringBuilder();
		for(int i = start; i < args.length; i++){
			builder.append(args[i] + " ");
		}
		return builder.toString().substring(0, builder.toString().length() - 1);
	}

	/**
	 * Puts a Collection of Strings into a 'cool' list. @author CakePvP
	 *
	 * @param list  list to translate
	 * @return      'cool-ified' list as String
	 */
	public static String toCoolList(Collection<String> list) {
		StringBuilder builder = new StringBuilder();
		int amount = 0;

		for(String str : list) {
			if(amount != 0) {
				builder.append(amount == list.size() - 1 ? " and " : ", ");
			}

			builder.append(str);
			amount++;
		}

		return builder.toString();
	}

}
