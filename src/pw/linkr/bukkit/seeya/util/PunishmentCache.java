package pw.linkr.bukkit.seeya.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import lombok.Getter;
import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.Punishment.PunishmentType;
import pw.linkr.bukkit.seeya.SQL;
import pw.linkr.bukkit.seeya.punishments.Ban;
import pw.linkr.bukkit.seeya.punishments.Mute;
import pw.linkr.bukkit.seeya.punishments.TempBan;

public class PunishmentCache {

	@Getter private static PunishmentCache instance = new PunishmentCache();

	@Getter private HashMap<String, Punishment> bans = new HashMap<String, Punishment>();

	public Ban getBan(String uuid){
		return (Ban) bans.get(uuid);
	}

	public void addBan(Punishment ban) {
		bans.put(ban.getUserUUID(), ban);
	}

	@Getter private HashMap<String, Punishment> tempbans = new HashMap<String, Punishment>();

	public TempBan getTempban(String uuid){
		return (TempBan)tempbans.get(uuid);
	}

	public void addTempban(Punishment tempban) {
		tempbans.put(tempban.getUserUUID(), tempban);
	}

	@Getter private HashMap<String, Punishment> mutes = new HashMap<String, Punishment>();

	public Mute getMute(String uuid){
		return (Mute) mutes.get(uuid);
	}

	public void addMute(Punishment mute) {
		mutes.put(mute.getUserUUID(), mute);
	}

	public void addPunishment(Punishment punishment) {
		switch (punishment.getType().getType()) {
			case BAN:
				addBan(punishment);
				break;
			case KICK: // No action
				break;
			case MUTE:
				addMute(punishment);
				break;
			case TEMPBAN:
				addTempban(punishment);
				break;
		}
	}

	public void fetchAll(SQL sql) throws SQLException {
		int bans = 0, mutes = 0, tempBans = 0;

		ResultSet set = sql.query("SELECT * FROM " + PunishmentType.BAN.getTableName() + " WHERE unbanned = '0' AND duration = '0'");
		while(set.next()){
			bans++;
			addBan(new Ban(set.getString("punisher"), set.getString("reason"), set.getString("username"), set.getString("uuiduser"), set.getString("uuidpunisher")));
		}

		set = sql.query("SELECT * FROM " + PunishmentType.TEMPBAN.getTableName() + " WHERE unbanned = '0' AND duration > 0");
		while(set.next()){
			tempBans++;
			addTempban(new TempBan(set.getString("punisher"), set.getString("reason"), set.getString("username"), set.getString("uuiduser"), set.getString("uuidpunisher"), set.getInt("duration")));
		}

		set = sql.query("SELECT * FROM " + PunishmentType.MUTE.getTableName() + " WHERE active = '1'");
		while(set.next()){
			mutes++;
			addMute(new Mute(set.getString("punisher"), set.getString("uuidpunisher"), set.getString("reason"), set.getString("username"), set.getString("uuiduser"), set.getInt("duration"), true));
		}
		
		Seeya.getInstance().logMessage(false, false, "Fetched " + bans + " ban" + (bans == 1 ? "" : "s") + ", " + tempBans + " temp ban" + (tempBans == 1 ? "" : "s") + " and " + mutes + " mute" + (mutes == 1 ? "" : "s") + " - active punishments from the database.");
		
	}

	public void unban(String uuid, String sender) {
		Seeya.getSQL().update("UPDATE " + PunishmentType.BAN.getTableName() + " SET unbanned = '1', unbanner = '" + sender + "', unbantime = UNIX_TIMESTAMP(now()) WHERE uuiduser = '" + uuid + "'");
		if(getBans().containsKey(uuid)){
			getBans().remove(uuid);
		}
		if(getTempbans().containsKey(uuid)){
			getTempbans().remove(uuid);
		}
	}


}
