package pw.linkr.bukkit.seeya.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

import org.bukkit.entity.Player;

public @Getter @AllArgsConstructor class Permissions {

	private Player player;

	public boolean isAdmin(){
		return player.isOp() || player.hasPermission("seeya.*");
	}

	public boolean can(String what){
		return isAdmin() || player.hasPermission("seeya." + what.toLowerCase());
	}

	public boolean canPunish(String what){
		return isAdmin() || player.hasPermission("seeya.punish.*") || player.hasPermission("seeya.punish." + what.toLowerCase());
	}

	public boolean exempt() {
		return canPunish("exempt ");
	}

}
