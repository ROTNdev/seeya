package pw.linkr.bukkit.seeya.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import pw.linkr.bukkit.seeya.Seeya;

public class Chat {

	/**
	 * Convert or clean up the string so that a database will not scream
	 * @return "cleaned" string
	 */
	public static String clean(String dirtyString){
		String cleanString = dirtyString;
		cleanString = cleanString.replaceAll("'", "''");
		return cleanString;
	}

	/**
	 * Convert database friendly string back to normal
	 * @return "mucked up" string
	 */
	public static String muckUpString(String cleanString){
		String dirtyString = cleanString;
		dirtyString = dirtyString.replaceAll("''", "'");
		return dirtyString;
	}

	/**
	 * Convert the string into the duration (in seconds) of the punishment
	 * @param sender - user executing command
	 * @param timeString - the input string
	 * @return an array - <br>
	 * 0 = the time in seconds <br>
	 * 1 = the readable time
	 */
	public static String[] calcLength(CommandSender sender, String timeString){
		String numberString = timeString.replace("m", "").replace("d", "").replace("s", "").replace("h", "").replace("w", "");
		int number = -1;
		try{
			number = Integer.parseInt(numberString);
		}catch(NumberFormatException ex){
			Seeya.getInstance().logMessage(false, false, "Halting calculation: Invalid number (" + numberString + ")");
			sender.sendMessage(ChatColor.DARK_PURPLE + "Please enter a valid number and unit.");
			sender.sendMessage(ChatColor.DARK_PURPLE + "Valid units: " + ChatColor.LIGHT_PURPLE + "s, m, h, d, w");
			return null;
		}

		if(number <= 0){
			sender.sendMessage(ChatColor.DARK_PURPLE + "Please enter a positive number.");
			return null;
		}

		String unit = "";
		for(char c : timeString.toCharArray()){
			if(!Character.isDigit(c)){
				unit = c + unit;
			}
		}

		if(unit.isEmpty()){
			Seeya.getInstance().logMessage(false, false, "Halting calculation: Unknown unit");
			sender.sendMessage(ChatColor.DARK_PURPLE + "Please enter a valid number and unit.");
			sender.sendMessage(ChatColor.DARK_PURPLE + "Valid units: " + ChatColor.LIGHT_PURPLE + "s, m, h, d, w");
			return null;
		}

		int length = getTime(unit, number);
		if(length <= 0){
			Seeya.getInstance().logMessage(false, false, "Halting calculation: Length is 0");
			sender.sendMessage(ChatColor.DARK_PURPLE + "Please enter a valid number and unit.");
			return null;
		}

		return new String[] { "" + length, number + " " + getLong(unit, number) };
	}

	/**
	 * Get a readable string using the length and the unit.
	 */
	public static String getLong(String inputString, int time) {
		if (inputString.equalsIgnoreCase("y"))
			return "year" + (time > 1 ? "s" : "");
		if (inputString.equalsIgnoreCase("mo"))
			return "month" + (time > 1 ? "s" : "");
		if (inputString.equalsIgnoreCase("w"))
			return "week" + (time > 1 ? "s" : "");
		if (inputString.equalsIgnoreCase("d"))
			return "day" + (time > 1 ? "s" : "");
		if (inputString.equalsIgnoreCase("h"))
			return "hour" + (time > 1 ? "s" : "");
		if (inputString.equalsIgnoreCase("m"))
			return "minute" + (time > 1 ? "s" : "");
		if (inputString.equalsIgnoreCase("s")) {
			return "second" + (time > 1 ? "s" : "");
		}
		return "unknown";
	}

	/**
	 * Get the duration using the unit and the amount of the unit (#Complicated)
	 */
	public static int getTime(String inputString, int original) {
		if (inputString.equalsIgnoreCase("y"))
			return original * 60 * 60 * 24 * 7 * 30 * 12;
		if (inputString.equalsIgnoreCase("mo"))
			return original * 60 * 60 * 24 * 7 * 30;
		if (inputString.equalsIgnoreCase("w"))
			return original * 60 * 60 * 24 * 7;
		if (inputString.equalsIgnoreCase("d"))
			return original * 60 * 60 * 24;
		if (inputString.equalsIgnoreCase("h"))
			return original * 60 * 60;
		if (inputString.equalsIgnoreCase("m"))
			return original * 60;
		if (inputString.equalsIgnoreCase("s")) {
			return original;
		}
		return 0;
	}

}
