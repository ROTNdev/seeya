package pw.linkr.bukkit.seeya.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.events.AutomaticUnbanEvent;
import pw.linkr.bukkit.seeya.events.BannedLoginEvent;
import pw.linkr.bukkit.seeya.punishments.Ban;
import pw.linkr.bukkit.seeya.punishments.TempBan;
import pw.linkr.bukkit.seeya.util.Chat;
import pw.linkr.bukkit.seeya.util.PunishmentCache;
import pw.linkr.bukkit.seeya.uuid.UUIDGetter;

public class CallListener implements Listener {

	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		Player player = e.getPlayer();
		String uuid = UUIDGetter.getUUID(player);

		if(PunishmentCache.getInstance().getBans().containsKey(uuid)){
			Ban playerBan = PunishmentCache.getInstance().getBan(uuid);
			String reason = Chat.muckUpString(playerBan.getReason());

			BannedLoginEvent apiEvent = new BannedLoginEvent(playerBan);
			Bukkit.getPluginManager().callEvent(apiEvent);
			e.disallow(Result.KICK_BANNED, ChatColor.translateAlternateColorCodes('&', Seeya.getLang().getBanJoin().replace("%name", player.getName()).replace("%reason", reason)));
		}

		if(PunishmentCache.getInstance().getTempbans().containsKey(uuid)){
			TempBan playerBan = PunishmentCache.getInstance().getTempban(uuid);
			long loggedTime = playerBan.getDuration();
			String reason = Chat.muckUpString(playerBan.getReason());

			if(System.currentTimeMillis() / 1000 <= loggedTime){
				BannedLoginEvent apiEvent = new BannedLoginEvent(playerBan);
				Bukkit.getPluginManager().callEvent(apiEvent);
				e.disallow(Result.KICK_BANNED, ChatColor.translateAlternateColorCodes('&', Seeya.getLang().getTempBanJoin().replace("%name", player.getName()).replace("%reason", reason).replace("%time", digitalTime(System.currentTimeMillis() /1000 - loggedTime))));
			} else {
				PunishmentCache.getInstance().unban(uuid, "Console-Automated");
				AutomaticUnbanEvent apiEvent = new AutomaticUnbanEvent(playerBan, false);
				Bukkit.getPluginManager().callEvent(apiEvent);
			}
		}
	}

	public static String digitalTime(long l){
		long hours = Math.abs(l / 3600);
		long min = Math.abs((l % 3600)/60);
		long sec =  Math.abs((l % 3600)%60);
		
		String shour = (hours < 10 ? "0" : "") + hours;
		String smin = (min < 10 ? "0" : "") + min;
	    String ssec = (sec < 10 ? "0" : "") + sec;
	    return shour+":"+smin + ":" + ssec;
	}

}
