package pw.linkr.bukkit.seeya.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pw.linkr.bukkit.seeya.Punishment;

public @Getter @AllArgsConstructor class AutomaticUnbanEvent extends BaseEvent {

	private Punishment punishment;
	private boolean inTimer;

}
