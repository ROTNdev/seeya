package pw.linkr.bukkit.seeya.commands.punish;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.commands.AbstractCommand;
import pw.linkr.bukkit.seeya.punishments.Kick;
import pw.linkr.bukkit.seeya.uuid.UUIDGetter;

public class KickCMD extends AbstractCommand {

	@Override
	public void execute(CommandSender sender, String tag, String[] args) {
		if(sender instanceof Player && !getPerms((Player)sender).canPunish("kick")){
			tell(sender, "&cYou can't use this command.");
			return;
		}
		if(args.length >= 2){

			if(Bukkit.getPlayer(args[0]) == null){
				tell(sender, "&e'" + args[0] + "' &5not found.");
				return;
			}

			Player player = Bukkit.getPlayer(args[0]);

			if(getPerms(player).exempt()){
				tell(sender, "&6" + player.getName() + "&5 can't be kicked.");
				return;
			}

			String reason = Seeya.getInstance().buildArgs(args, 1);
			String punisherUUID = sender.getName().equalsIgnoreCase("CONSOLE") ? sender.getName() : UUIDGetter.getUUID(((Player)sender));
			
			Punishment punishment = new Kick(sender.getName(), punisherUUID, reason, player.getName(), UUIDGetter.getUUID(player));
			punishment.logToSQL();
			punishment.notifyPunishment();
			player.kickPlayer(ChatColor.translateAlternateColorCodes('&', Seeya.getLang().getKick().replace("%reason", reason.replaceAll("%", "%%")).replace("%kicker", sender.getName())));
			tell(sender, "You have kicked &e" + player.getName() + "&5 for " + reason + ".");

		} else {
			tell(sender, "Usage: &e/" + tag + " <username> <reason..>");
		}
	}

	public boolean playerOnly() {
		return false;
	}

}
