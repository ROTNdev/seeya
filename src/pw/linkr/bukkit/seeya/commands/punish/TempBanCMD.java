package pw.linkr.bukkit.seeya.commands.punish;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.commands.AbstractCommand;
import pw.linkr.bukkit.seeya.punishments.TempBan;
import pw.linkr.bukkit.seeya.util.Chat;
import pw.linkr.bukkit.seeya.util.PunishmentCache;
import pw.linkr.bukkit.seeya.uuid.UUIDGetter;

public class TempBanCMD extends AbstractCommand {

	@Override
	public void execute(CommandSender sender, String tag, String[] args) {
		if(sender instanceof Player && !getPerms((Player)sender).canPunish("tempban")){
			tell(sender, "&cYou can't use this command.");
			return;
		}
		if(args.length >= 3){

			tell(sender, "&7Looking up user..");
			String uuid = UUIDGetter.getUUID(args[0]);

			if(uuid == null){
				tell(sender, "&e'" + args[0] + "'&5 is not a valid username.");
				return;
			}

			if(PunishmentCache.getInstance().getBans().containsKey(uuid) || PunishmentCache.getInstance().getTempbans().containsKey(uuid)){
				tell(sender, "That player is already banned!");
				return;
			}

			boolean online = Bukkit.getPlayer(args[0]) != null;

			if((online && getPerms(Bukkit.getPlayer(args[0])).exempt()) || (!online && Bukkit.getOfflinePlayer(args[0]).isOp())){
				tell(sender, "&6" +  (online ? Bukkit.getPlayer(args[0]).getName() : args[0]) + "&5 can't be banned.");
				return;
			}

			String timeString = args[1];
			String[] lengthArray = Chat.calcLength(sender, timeString);
			if(lengthArray == null){
				// No message will be sent here the calcLength method will handle that.
				return;
			}

			int length = Integer.parseInt(lengthArray[0]);

			String reason = Seeya.getInstance().buildArgs(args, 2);
			String punisherUUID = sender.getName().equalsIgnoreCase("CONSOLE") ? sender.getName() : UUIDGetter.getUUID(((Player)sender));

			Punishment punishment = new TempBan(sender.getName(), reason, args[0], uuid, punisherUUID, length);
			punishment.logToSQL();
			punishment.notifyPunishment();

			if(Bukkit.getPlayer(args[0]) != null){
				Bukkit.getPlayer(args[0]).kickPlayer(ChatColor.translateAlternateColorCodes('&', Seeya.getLang().getTempBan().replace("%reason", reason.replaceAll("%", "%%")).replace("%time", lengthArray[1]).replace("%kicker", sender.getName())));
			}
			tell(sender, "You have banned &6'" + args[0] + "'&5 for &e" + reason + "&5, duration: &c" + lengthArray[1] + "&5.");
			if(online){
				tell(sender, "They have also been kicked from the server.");
			}

		} else {
			tell(sender, "Usage: &e/" + tag + " <username> <time:unit> <reason..>");
		}
	}

	public boolean playerOnly() {
		return false;
	}

}
