package pw.linkr.bukkit.seeya.commands.punish;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.commands.AbstractCommand;
import pw.linkr.bukkit.seeya.util.PunishmentCache;
import pw.linkr.bukkit.seeya.uuid.UUIDGetter;

public class UnbanCMD extends AbstractCommand {

	@Override
	public void execute(CommandSender sender, String tag, String[] args) {
		if(sender instanceof Player && !getPerms((Player)sender).canPunish("unban")){
			tell(sender, "&cYou can't use this command.");
			return;
		}
		if(args.length == 1){

			tell(sender, "&7Looking up user..");
			String uuid = UUIDGetter.getUUID(args[0]);

			if(uuid == null){
				tell(sender, "&e'" + args[0] + "'&5 is not a valid username.");
				return;
			}

			if(!PunishmentCache.getInstance().getBans().containsKey(uuid) && !PunishmentCache.getInstance().getTempbans().containsKey(uuid)){
				tell(sender, "That player isn't banned!");
				return;
			}

			PunishmentCache.getInstance().unban(uuid, sender.getName());

			tell(sender, "You have unbanned &6'" + args[0] + "'&5.");

		} else {
			tell(sender, "Usage: &e/" + tag + " <username>");
		}
	}

	public boolean playerOnly() {
		return false;
	}

}
