package pw.linkr.bukkit.seeya.commands.punish;

import java.sql.SQLException;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.commands.AbstractCommand;
import pw.linkr.bukkit.seeya.util.Permissions;
import pw.linkr.bukkit.seeya.util.PunishmentCache;

public class SeeyaCMD extends AbstractCommand {

	@Override
	public void execute(CommandSender sender, String tag, String[] args) {
		Player player = (Player)sender;
		Permissions perms = getPerms(player);
		if(args.length == 0){
			tell(sender, "Seeya &av" + Seeya.getInstance().getDescription().getVersion() + "&5, by &e" + Seeya.toCoolList(Seeya.getInstance().getDescription().getAuthors()));
			tell(sender, "For a command reference visit &bhttps://linkr.pw/seeya");
		} else if(args.length == 1){
			if(args[0].equalsIgnoreCase("recache") && perms.can("admin.recache")){
				try{
					tell(sender, "&aContacting database and pulling..");
					PunishmentCache.getInstance().fetchAll(Seeya.getSQL());
					tell(sender, "&aRecache was successful.");
				}catch(SQLException ex){
					tell(sender, "&cSomething went wrong whilst performing that action, please try again later or contact an admin");
				}
			} else if(args[0].equalsIgnoreCase("reload") && perms.can("admin.reload")){
				Seeya.getInstance().reloadConfigs();
				tell(sender, "&aReloaded Seeya's lang.yml and settings.yml");
			} else {
				tell(sender, "Seeya &v" + Seeya.getInstance().getDescription().getVersion() + "&5, by &e" + Seeya.toCoolList(Seeya.getInstance().getDescription().getAuthors()));
			}
		} else {
			tell(sender, "Seeya &v" + Seeya.getInstance().getDescription().getVersion() + "&5, by &e" + Seeya.toCoolList(Seeya.getInstance().getDescription().getAuthors()));
		}
	}

	public boolean playerOnly() {
		return true;
	}

}
