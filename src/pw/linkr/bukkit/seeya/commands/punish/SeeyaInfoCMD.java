package pw.linkr.bukkit.seeya.commands.punish;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.commands.AbstractCommand;
import pw.linkr.bukkit.seeya.util.PunishmentCache;
import pw.linkr.bukkit.seeya.uuid.UUIDGetter;

public class SeeyaInfoCMD extends AbstractCommand {

	@Override
	public void execute(CommandSender sender, String tag, String[] args) {
		Player player = (Player)sender;
		if(!getPerms(player).can("info.userinfo")){
			tell(sender, "&cYou can't use this command.");
			return;
		}
		if(args.length == 1){
			String uuid = UUIDGetter.getUUID(args[0]);

			if(uuid == null){
				tell(sender, "&e'" + args[0] + "'&5 is not a valid username.");
				return;
			}

			boolean isBanned = PunishmentCache.getInstance().getBans().containsKey(uuid) || PunishmentCache.getInstance().getTempbans().containsKey(uuid);
			tell(sender, "&aInfo of &6" + args[0] + "&a.");
			tell(sender, "Banned? " + (isBanned ? "&cYes" : "&aNo"));
			tell(sender, "This command is incomplete because the plugin is not complete.");
			tell(sender, "When /mute and /unmute is added (next update) this command will be complete!");
			tell(sender, "Until then watch &bhttps://linkr.pw/seeya&5 for updates.");

		} else {
			tell(sender, "Usage: &e/" + tag + " <username>");
		}
	}

	public boolean playerOnly() {
		return true;
	}

}
