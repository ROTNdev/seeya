package pw.linkr.bukkit.seeya.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.util.Permissions;

public abstract class AbstractCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String tag, String[] args) {
		if(playerOnly() && !(cs instanceof Player)){
			tell(cs, "&cOnly players can use this command.");
			return false;
		}

		execute(cs, tag, args);
		return false;
	}

	public abstract boolean playerOnly();
	public abstract void execute(CommandSender sender, String tag, String[] args);

	protected Permissions getPerms(Player player){
		return new Permissions(player);
	}

	protected void tell(CommandSender sender, String msg){
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&5" + msg.replaceAll("%", "%%")));
	}

}
