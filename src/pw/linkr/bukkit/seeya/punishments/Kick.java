package pw.linkr.bukkit.seeya.punishments;

import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;

public class Kick extends Punishment {

	public Kick(String punisher, String uuidPunisher, String reason, String affectedUser, String uuidUser){
		super(punisher, uuidPunisher, reason, affectedUser, uuidUser, PunishmentType.KICK);
	}

	@Override
	public String getValues() {
		return "'" + getAffectedUser() + "', '" + getPunisher() + "', '" + getReason() + "', UNIX_TIMESTAMP(now()), '" + Seeya.getSettings().getServer() + "'";
	}

	@Override
	public String getNotifyString() {
		return "&b" + getAffectedUser() + " &5has been kicked by &e" + getPunisher() + "&5 for &d" + getReason() + "&5.";
	}

}
