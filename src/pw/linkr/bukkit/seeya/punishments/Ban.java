package pw.linkr.bukkit.seeya.punishments;

import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;

public class Ban extends Punishment {

	public Ban(String punisher, String reason, String affectedUser, String uuidUser, String uuidPunisher) {
		super(punisher, uuidPunisher, reason, affectedUser, uuidUser, PunishmentType.BAN);
	}

	@Override
	public String getValues() {
		return "'" + getAffectedUser() + "', '" + getPunisher() + "', '" + getReason() + "', UNIX_TIMESTAMP(now()), '0', '0', '0', 'none', '" + Seeya.getSettings().getServer() + "'";
	}

	@Override
	public String getNotifyString() {
		return "&b" + getAffectedUser() + " &5has been banned by &e" + getPunisher() + "&5 for &d" + getReason() + "&5.";
	}

}