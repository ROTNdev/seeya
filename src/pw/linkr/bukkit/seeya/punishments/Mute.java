package pw.linkr.bukkit.seeya.punishments;

import lombok.Getter;
import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.listener.CallListener;

public class Mute extends Punishment {

	@Getter private int duration;
	@Getter private boolean isActive;

	public Mute(String punisher, String uuidPunisher, String reason, String affectedUser, String uuidUser, int duration, boolean active) {
		super(punisher, uuidPunisher, reason, affectedUser, uuidUser, PunishmentType.MUTE);
		this.duration = duration;
		this.isActive = active;
	}

	@Override
	public String getValues() {
		return "'" + getAffectedUser() + "', '" + getPunisher() + "', '" + getReason() + "', UNIX_TIMESTAMP(now()), '" + (System.currentTimeMillis() / 1000 + duration) +"', '1', '" + Seeya.getSettings().getServer() + "'";
	}

	@Override
	public String getNotifyString() {
		return "�b" + getAffectedUser() + " �5has been muted by �e" + getPunisher() + "�5 for " + getReason() + ", duration: " + CallListener.digitalTime(System.currentTimeMillis() /1000 - getDuration()) + ".";
	}

}
