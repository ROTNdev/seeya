package pw.linkr.bukkit.seeya.punishments;

import lombok.Getter;
import lombok.Setter;
import pw.linkr.bukkit.seeya.Punishment;
import pw.linkr.bukkit.seeya.Seeya;
import pw.linkr.bukkit.seeya.listener.CallListener;

public class TempBan extends Punishment {

	@Setter @Getter private long duration;

	public TempBan(String punisher, String reason, String affectedUser, String uuidUser, String uuidPunisher, int duration) {
		super(punisher, uuidPunisher, reason, affectedUser, uuidUser, PunishmentType.TEMPBAN);
		this.duration = duration;
	}

	@Override
	public String getValues() {
		return "'" + getAffectedUser() + "', '" + getPunisher() + "', '" + getReason() + "', UNIX_TIMESTAMP(now()), '" + ((System.currentTimeMillis() / 1000) + getDuration()) + "', '0', '0', 'none', '" + Seeya.getSettings().getServer() + "'";
	}

	@Override
	public String getNotifyString() {
		return "�b" + getAffectedUser() + " �5has been tempbanned by �e" + getPunisher() + "�5 for &d" + getReason() + "&5, duration: &c" + CallListener.digitalTime(getDuration()) + "&5.";
	}

}