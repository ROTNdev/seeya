package pw.linkr.bukkit.seeya.uuid;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.mojang.api.profiles.HttpProfileRepository;
import com.mojang.api.profiles.Profile;
import com.mojang.api.profiles.ProfileCriteria;

public class UUIDGetter {

	private static HttpProfileRepository profileRepository = new HttpProfileRepository();

	public static String getUUID(Player player){
		if(Bukkit.getServer().getOnlineMode()){
			return translateEntityUUID(player);
		} else {
			return getUUID(player.getName());
		}
	}

	/**
	 * @author Mojang
	 * Get a UUID from a username.
	 */
	public static String getUUID(String name){
        Profile[] profiles = profileRepository.findProfilesByCriteria(new ProfileCriteria(name, "minecraft"));

        if(profiles.length > 0) {
            return profiles[0].getId();
        } else {
            return null;
        }
    }

	/**
	 * Convert an entity uuid to a mojang one (remove the -'s)
	 */
	private static String translateEntityUUID(Player player){
		return player.getUniqueId().toString().replaceAll("-", "");
	}

}
