package pw.linkr.bukkit.seeya;

import lombok.AllArgsConstructor;
import lombok.Getter;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import pw.linkr.bukkit.seeya.Punishment.PunishmentType.OtherPunishmentType;
import pw.linkr.bukkit.seeya.listener.CallListener;
import pw.linkr.bukkit.seeya.punishments.TempBan;
import pw.linkr.bukkit.seeya.util.Chat;
import pw.linkr.bukkit.seeya.util.Permissions;
import pw.linkr.bukkit.seeya.util.PunishmentCache;

public abstract @Getter @AllArgsConstructor class Punishment {

	public static @Getter @AllArgsConstructor class PunishmentType {
		
		public enum OtherPunishmentType {
			BAN, MUTE, TEMPBAN, KICK;
		}

		private String tableName;
		private OtherPunishmentType type;
		public static PunishmentType BAN, MUTE, TEMPBAN, KICK;
		
		public static void init(FileConfiguration config){
			String prefix = config.getString("sql.prefix");

			BAN = new PunishmentType(prefix + "bans", OtherPunishmentType.BAN);
			TEMPBAN = new PunishmentType(prefix + "bans", OtherPunishmentType.TEMPBAN);
			MUTE = new PunishmentType(prefix + "mutes", OtherPunishmentType.MUTE);
			KICK = new PunishmentType(prefix + "kicks", OtherPunishmentType.KICK);
		}
		
	}

	private String punisher, punisherUUID;
	private String reason;
	private String affectedUser, userUUID;
	private PunishmentType type;

	/**
	 * Insert the punishment into the database and cache
	 */
	public void logToSQL(){
		reason = Chat.clean(reason);

		StringBuilder builder = new StringBuilder();
		builder.append("INSERT INTO " + type.getTableName() + " (");
		/**
		 * Get the columns
		 */
		switch(type.getType()){
			case BAN:
				builder.append("username, punisher, reason, time, duration, unbanned, unbantime, unbanner, server, uuiduser, uuidpunisher");
				break;
			case KICK:
				builder.append("username, punisher, reason, time, server, uuiduser, uuidpunisher");
				break;
			case TEMPBAN:
				builder.append("username, punisher, reason, time, duration, unbanned, unbantime, unbanner, server, uuiduser, uuidpunisher");
				break;
			case MUTE:
				builder.append("username, punisher, reason, time, duration, active, server, uuiduser, uuidpunisher");
				break;
		}
		builder.append(") VALUES (");
		/**
		 * Insert values
		 */
		builder.append(getValues());
		/**
		 * Add UUIDs (To prevent people from changing name to bypass bans)
		 */
		builder.append(", '" + getUserUUID() + "', '" + getPunisherUUID() + "');");
		/**
		 * Execute
		 */
		Seeya.getSQL().update(builder.toString());

		if(getType().getType() == OtherPunishmentType.TEMPBAN){
			((TempBan)this).setDuration((System.currentTimeMillis() / 1000) + ((TempBan)this).getDuration());
		}
		PunishmentCache.getInstance().addPunishment(this);
	}

	/**
	 * Get the values for the punishment
	 */
	public abstract String getValues();

	/**
	 * The info string about the punishment.
	 */
	public abstract String getNotifyString();

	/**
	 * Notify users with permissions about the punishment (and log to console)
	 */
	public void notifyPunishment(){
		for(Player players : Bukkit.getOnlinePlayers()){
			if(new Permissions(players).can("info.notify")){
				players.sendMessage(ChatColor.DARK_PURPLE + ChatColor.translateAlternateColorCodes('&', getNotifyString()));
			}
		}

		if(getType().getType() == PunishmentType.TEMPBAN.getType()){
			Seeya.getInstance().logMessage(true, true, getAffectedUser() + " has been punished:", "Type: " + getType().getType().name(), "Reason: " + getReason(), "Punisher: " + getPunisher(), "Duration: " + CallListener.digitalTime(System.currentTimeMillis() / 1000 - (System.currentTimeMillis() / 1000 + ((TempBan)this).getDuration())));
		} else {
			Seeya.getInstance().logMessage(true, true, getAffectedUser() + " has been punished:", "Type: " + getType().getType().name(), "Reason: " + getReason(), "Punisher: " + getPunisher());
		}
	}

}
