package com.mojang.api.http;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public @AllArgsConstructor class HttpHeader {

	@Getter @Setter private String name;
	@Getter @Setter private String value;

}