package com.mojang.api.profiles;

import lombok.Getter;
import lombok.Setter;

public class ProfileSearchResult {

	@Getter @Setter private Profile[] profiles;
	@Getter @Setter private int size;

}