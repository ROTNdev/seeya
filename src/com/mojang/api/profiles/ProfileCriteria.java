package com.mojang.api.profiles;

import lombok.AllArgsConstructor;
import lombok.Getter;

public @AllArgsConstructor class ProfileCriteria {

	@Getter private final String name;
	@Getter private final String agent;

}