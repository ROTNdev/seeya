package com.mojang.api.profiles;

import lombok.Getter;
import lombok.Setter;

public class Profile {

	@Getter @Setter private String id;
	@Getter @Setter private String name;

}
